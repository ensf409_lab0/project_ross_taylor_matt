// converter.class

public class Converter {
//Your names go here:
/*
<<<<<<< HEAD
* @Author: Ross Bartlett
* Matthew Wiens
* Taylor Huang
*
*/
private static double celsiusToFahrenheit(double C){
return (1.8*C) + 32;
}
private static double fahrenheitToCelsius(double F){
	return (F-32)/1.8;
}
public static void main(String[] args) {
//TODO: The first student will implement this method.
// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
    
    double f = celsiusToFahrenheit(180);
    double c =  fahrenheitToCelsius(250);
    
    System.out.println("180 Celcius in Farenheit is: "+f);
    System.out.println("250 Fahrenheit in Celsius is: "+c);
    
}
}
